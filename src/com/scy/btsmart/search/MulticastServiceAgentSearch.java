package com.scy.btsmart.search;

import java.util.ArrayList;
import java.util.List;

import com.scy.btsmart.common.BtSmartCommon;
import com.scy.btsmart.common.NetUtil;

public class MulticastServiceAgentSearch {
	
	 private static List<String> mBtSmartAgentList = new ArrayList<String>(); 
	 
	 private static MulticastService mService = null;
	 
	 
	 public static void searchBtSmartAgent(){
		 //取组播服务
		 mService = NetUtil.createMulticastService("239.255.255.250", 1901, null);
		 
		 //发送数据
		 mService.sendMulticastData(BtSmartCommon.SCY);	
		 
		 //启动服务
		 mService.start();
	 }
	 
	 //停止搜索
	 public static void stopSearch(){
		 if(mService != null){
			 mService.stopServer();
		 }
	 }
	 
	 public static List<String> getBtSmartAgent(){
		 return mBtSmartAgentList;
	 }
	 
	 public static void addBtSmartAgent(String ip){
		 if(ip != null){
			 mBtSmartAgentList.add(ip);
		 }
	 }
}
