package com.scy.btsmart.http.httpmessage;

import java.util.ArrayList;
import java.util.List;

public class HttpGetDeviceListRsp extends HttpMessageRsp{
    
    private List<DeviceItem> mDeviceList;
    
    public class DeviceItem{
    	
    	private String  mName;
    	
    	private String  mMac;
    	
    	private int  mRssi;
    	
    	public DeviceItem(){
    	}

		public String getmName() {
			return mName;
		}

		public void setmName(String mName) {
			this.mName = mName;
		}

		public String getmMac() {
			return mMac;
		}

		public void setmMac(String mMac) {
			this.mMac = mMac;
		}

		public int getmRssi() {
			return mRssi;
		}

		public void setmRssi(int mRssi) {
			this.mRssi = mRssi;
		}
    }

	public HttpGetDeviceListRsp(){
		
	}

	public List<DeviceItem> getmDeviceList() {
		return mDeviceList;
	}

	public void setmDeviceList(List<DeviceItem> mDeviceList) {
		this.mDeviceList = new ArrayList<DeviceItem>(mDeviceList.size());  
    	for(DeviceItem uuid : mDeviceList){
    		this.mDeviceList.add(uuid);  
    	} 
	}
}
