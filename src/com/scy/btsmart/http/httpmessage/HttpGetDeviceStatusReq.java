package com.scy.btsmart.http.httpmessage;

public class HttpGetDeviceStatusReq extends HttpMessageReq{
	
	private String   mName;
	
	private String   mMac;

	public HttpGetDeviceStatusReq(){
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmMac() {
		return mMac;
	}

	public void setmMac(String mMac) {
		this.mMac = mMac;
	}
}
