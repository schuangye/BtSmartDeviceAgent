package com.scy.btsmart.http.httpmessage;

public class HttpMessageReq{
	private String   mClientID;
	
	public HttpMessageReq(){
		mClientID = "";
	}

	public String getmClientID() {
		return mClientID;
	}

	public void setmClientID(String mClientID) {
		this.mClientID = mClientID;
	}

}
