package com.scy.btsmart.http.httphandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

import android.util.Log;

import com.scy.btsmart.bluetooth.BtSmartInterfaceManage;
import com.scy.btsmart.common.HttpUtil;
import com.scy.btsmart.common.JsonUtil;
import com.scy.btsmart.conf.BtSmartDeviceServerItem;
import com.scy.btsmart.conf.BtSmartDeviceServerItem.BtCharacteristic;
import com.scy.btsmart.http.httpmessage.HttpGetDeviceStatusReq;
import com.scy.btsmart.http.httpmessage.HttpQueryCharacteristicListRsp;
import com.scy.btsmart.http.httpmessage.HttpQueryCharacteristicListRsp.ServerItem;
import com.scy.btsmart.http.httpmessage.HttpQueryCharacteristicListRsp.ServerItem.CharacteristicItem;

public class QueryCharacteristicListHandler implements HttpRequestHandler{

	private static final String TAG = "QueryCharacteristicHandler";
	
	public QueryCharacteristicListHandler(){
	}
	
	@Override
	public void handle(HttpRequest request, HttpResponse response, HttpContext context)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		 String body = HttpUtil.getBodyFromRequest(request);
		 
		 //获取req对象
		 HttpGetDeviceStatusReq req = (HttpGetDeviceStatusReq)JsonUtil.json2Object(body, HttpGetDeviceStatusReq.class);
		 Log.i(TAG, "req:" + req.getmClientID()); 
	
		 
		 HttpQueryCharacteristicListRsp rsp = FillCharacteristicList(req);
		 
		 //构造rspjson
		 String rspjson = JsonUtil.object2Json(rsp);
		 
		 //返回结果
		 response.setStatusCode(HttpStatus.SC_OK);  
         StringEntity entity = new StringEntity(rspjson);  
         response.setEntity(entity);
	}
	
	//填充属性列表
	private HttpQueryCharacteristicListRsp FillCharacteristicList(HttpGetDeviceStatusReq req){
		HttpQueryCharacteristicListRsp rsp = new HttpQueryCharacteristicListRsp();
		 rsp.setmErrorCode(0);
		 rsp.setmErrorDes("Success");
		List<BtSmartDeviceServerItem> tmplist = BtSmartInterfaceManage.getDeviceCharacteristicList(req.getmMac());
		if(tmplist.isEmpty()){
			 rsp.setmErrorCode(-1);
			 rsp.setmErrorDes("Fail");
			 return rsp;
		}
		
		List<ServerItem> mServerList = new ArrayList<ServerItem>();	
		for(BtSmartDeviceServerItem device : tmplist){
			ServerItem tmpser = rsp.new ServerItem();
			tmpser.setmServer(device.m_ServerName);
			tmpser.setmSUUID(device.m_ServerUUID.toString());
			
			List<CharacteristicItem> mCharaList = new ArrayList<CharacteristicItem>();
			for(BtCharacteristic chara : device.m_CharacteristicList){
				CharacteristicItem tmpchara = tmpser.new CharacteristicItem();
				tmpchara.setmName(chara.m_Name);
				tmpchara.setmDes(chara.m_Des);
				tmpchara.setmUUID(chara.m_UUID.toString());
				
				mCharaList.add(tmpchara);
			}
			
			tmpser.setmCharaList(mCharaList);
			mServerList.add(tmpser);
		}
		
		rsp.setmServerList(mServerList);
		
		return rsp;
	}

}
