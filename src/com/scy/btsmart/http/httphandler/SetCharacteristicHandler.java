package com.scy.btsmart.http.httphandler;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;

import android.util.Log;

import com.scy.btsmart.bluetooth.BtSmartInterfaceManage;
import com.scy.btsmart.common.BtSmartErrorCode;
import com.scy.btsmart.common.HttpUtil;
import com.scy.btsmart.common.JsonUtil;
import com.scy.btsmart.http.httpmessage.HttpMessageRsp;
import com.scy.btsmart.http.httpmessage.HttpSetCharacteristicReq;

public class SetCharacteristicHandler implements HttpRequestHandler{
	
	private static final String TAG = "SetCharacteristicHandler";
	
	public SetCharacteristicHandler(){
	}
	
	@Override
	public void handle(HttpRequest request, HttpResponse response, HttpContext context)
			throws HttpException, IOException {
		// TODO Auto-generated method stub
		String body = HttpUtil.getBodyFromRequest(request);
		 //获取req对象
		HttpSetCharacteristicReq req = (HttpSetCharacteristicReq)JsonUtil.json2Object(body, HttpSetCharacteristicReq.class);
		
	    //转换值
		byte []tmpvalue = JsonUtil.decodeBinary(req.getmValue());
		boolean ret = BtSmartInterfaceManage.synSetCharacteristic(req.getmMac(), req.getmSUUID(), req.getmUUID(), tmpvalue);
		
		 //构造返回值
		HttpMessageRsp rsp = new HttpMessageRsp();
		if(ret){
			rsp.setmErrorCode(0);
			rsp.setmErrorDes("Success");	
			Log.i(TAG, "BtSmartInterfaceManage.synSetCharacteristic"); 
		}else{
			rsp.setmErrorCode(BtSmartErrorCode.CHARACTERISTIC_WRITE_FAIL);
			rsp.setmErrorDes("fail");
		}
 
		 //构造rspjson
		String rspjson = JsonUtil.object2Json(rsp);
		 
		 //返回结果
		response.setStatusCode(HttpStatus.SC_OK);  
        StringEntity entity = new StringEntity(rspjson);  
        response.setEntity(entity);  
	}

}
