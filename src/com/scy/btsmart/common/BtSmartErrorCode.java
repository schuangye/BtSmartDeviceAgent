package com.scy.btsmart.common;

public class BtSmartErrorCode {
	
	public static final int BT_SMART_SUCCEED = 2000; 
	
	//�����붨��
	public static final int ENABLE_NOTIFY_FAIL = 2000; 
	public static final String ENABLE_NOTIFY_FAIL_DES = "enable notify fail"; 
	
	public static final int SERVICE_UUID_NOEXIST = 2001;
	public static final int CHARACTERISTIC_UUID_NOEXIST = 2002; 
	public static final int CHARACTERISTIC_READ_FAIL = 2003; 
	public static final int CHARACTERISTIC_WRITE_FAIL = 2004; 
	public static final String CHARACTERISTIC_WRITE_FAIL_DES = "set characteristic fail"; 
	
	public static final int DESCRIPTOR_UUID_NOEXIST = 2005; 
	public static final int DESCRIPTOR_READ_FAIL = 2006; 
	public static final int DESCRIPTOR_WRITE_FAIL = 2007; 
	
	public static final int READ_RSSI_FAIL = 2008; 
	
	public static final int TIME_OUT = 2009;

}
