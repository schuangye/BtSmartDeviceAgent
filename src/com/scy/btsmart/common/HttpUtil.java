package com.scy.btsmart.common;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

public class HttpUtil{

	//服务注册
	public static final String CMD_REGISTER  = "register";
	
	//设备扫描
	public static final String CMD_DEVICESCAN = "deviceScan";
	
	//获取设备列表
	public static final String CMD_GETDEVICELIST = "getDeviceList";
	
	//获取设备状态
	public static final String CMD_GETDEVICESTATUS = "getDeviceStatus";
	
	//查询设备支持的属性列表
	public static final String CMD_QUERY_CHARACTERISTIC_LIST = "queryCharacteristicList";
	
	//查询设备支持的属性值
	public static final String CMD_QUERY_CHARACTERISTIC = "queryCharacteristic";
	 
	//设备设备属性
	public static final String CMD_SET_CHARACTERISTIC = "setCharacteristic";
	
	//取httphandler名利
	public static String getHttpHandlerCmd(String cmd){
		return "/" + cmd;
	}
	
	//从请求中取body
	public static String getBodyFromRequest(HttpRequest request) throws IllegalStateException, IOException{
		HttpEntity req = ((BasicHttpEntityEnclosingRequest)request).getEntity();
		int length = (int)req.getContentLength();                
        InputStream inputStream=req.getContent();
        byte array[] = new byte[length];
        inputStream.read(array);
        String body = new String(array,"UTF-8");
        body = URLDecoder.decode(body,"UTF-8");
        return body;
	}
	
	//从相应中去body
	public static String getBodyFromResponse(HttpResponse response) throws IllegalStateException, IOException{
		HttpEntity rsp = response.getEntity();
		int length = (int)rsp.getContentLength();                
        InputStream inputStream = rsp.getContent();
        byte array[] = new byte[length];
        inputStream.read(array);
        String body = new String(array,"UTF-8");
        body = URLDecoder.decode(body,"UTF-8");
        return body;
	}
}
