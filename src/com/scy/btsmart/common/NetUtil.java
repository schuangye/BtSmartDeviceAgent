package com.scy.btsmart.common;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import org.apache.http.conn.util.InetAddressUtils;

import com.scy.btsmart.search.MulticastService;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.os.Handler;
import android.util.Log;

public class NetUtil {

	private static final String TAG = "MulticastNetUtil";
	private static MulticastLock mMulticastLock;
	
	//开启组播
	public static void enableMulticast(Context context){
		 WifiManager wifiManager=(WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		 if(wifiManager != null){
			 mMulticastLock = wifiManager.createMulticastLock("MulticastNetUtil.ts"); 
			 if(mMulticastLock != null){
				 mMulticastLock.acquire(); 
			 }	 
		 }
	}
	
	//禁用组播
	public static void disenableMulticast(){
		 if(mMulticastLock != null){
			 mMulticastLock.release(); 
		 }
	}
	
	//开启组播服务
	@SuppressWarnings("resource")
	public static MulticastService createMulticastService(String groupip, int mulport, Handler handler){
		MulticastSocket socket = null;
        InetAddress tmpgroupip = null; 
        
        try {
			tmpgroupip = InetAddress.getByName(groupip);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "InetAddress.getByName(groupip) fail. "+ e.getMessage()); 
			return null;
		}

        //创建多播socket
        try {
			socket = new MulticastSocket(mulport);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "socket = new MulticastSocket(mulport) fail. "+ e.getMessage()); 
			return null;
		}
        
        //加入组播组
        try {
			socket.joinGroup(tmpgroupip);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "socket.joinGroup(tmpgroupip) fail. "+ e.getMessage()); 
			return null;
		}
        
        try {
        	socket.setSoTimeout(3000);
			socket.setTimeToLive(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "socket.setTimeToLive(1);. "+ e.getMessage()); 
		}
        
		return new MulticastService(socket, tmpgroupip, mulport, handler);
	}
	
	//获取本机地址
	public static String getLocalHostIp(){
		String ipaddress = "";
		Enumeration<NetworkInterface> en;
		try {
			en = NetworkInterface.getNetworkInterfaces();
			//遍历所用的网络接口
			while (en.hasMoreElements()){
				
				// 得到每一个网络接口绑定的所有ip
				NetworkInterface nif = en.nextElement();
				Enumeration<InetAddress> inet = nif.getInetAddresses();
				while (inet.hasMoreElements()){
					InetAddress ip = inet.nextElement();
			        if (!ip.isLoopbackAddress() && InetAddressUtils.isIPv4Address(ip.getHostAddress()))
	                {
	                    return ip.getHostAddress();
	                }
				}
			}
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "en = NetworkInterface.getNetworkInterfaces() fail. "+ e.getMessage()); 
		}
		
		return ipaddress;
	}
	
}





