package com.scy.btsmart;


import com.scy.btsmart.bluetooth.BtSmartInterfaceManage;
import com.scy.btsmart.common.JsonUtil;
import com.scy.btsmart.search.MulticastServiceAgentSearch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		TestEncodeBinary();
		
		//初始化蓝牙接口
		BtSmartInterfaceManage.StartServer(this.getApplicationContext());
		
		//启动扫描服务
		StartService(com.scy.btsmart.http.httpserver.SystemHttpService.class);
		
		//启动广播服务
		StartService(com.scy.btsmart.search.SystemMulticastService.class);
		
		//显示界面
		setContentView(R.layout.activity_main);
	}
	
    @Override
    public void onDestroy() {
       
    	//解除绑定服务
    	BtSmartInterfaceManage.unBindService(this.getApplicationContext());
    	
        super.onDestroy();
    }
    
	private void StartService(Class service){
		Intent intent = new Intent(this, service); 
		this.startService(intent);
	}
	
	private void TestEncodeBinary(){
		
		byte []binbuff = {0,1,2,3,4,5,6,7,8,9,10,11,12,100};
		String binstr = JsonUtil.encodeBinary(binbuff);
		Log.i("TestEncodeBinary", "TestEncodeBinary:"+ binstr);
		
		byte []descode = JsonUtil.decodeBinary(binstr);
	}
}
