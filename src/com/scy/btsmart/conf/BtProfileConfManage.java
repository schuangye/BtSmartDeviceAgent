package com.scy.btsmart.conf;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.scy.btsmart.conf.BtSmartDeviceServerItem.BtCharacteristic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

@SuppressLint("NewApi")
public class BtProfileConfManage {

	private static final String TAG = "BtProfileConfManage";
	private static final String BT_SMART_KEY = "BtSmartLibaray";
	
	//保存服务器配置文件
	private static Map<UUID, BtSmartDeviceServerItem> mBtSmartMap = new HashMap<UUID, BtSmartDeviceServerItem>(); 
	private static Map<UUID, String>        mBtSmartUUIDMap = new HashMap<UUID, String>();
	
	public static int loadBtProfile(Context context)
	{
    	//读取配置文件
    	try {
			if(!FromXMLLoadBtProfile(context, "btProfile.xml")){
				Log.i(TAG, "Load btProfile.xml fail.");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "Load bt profile fail" + e.getMessage());
			return -1;
		}
    	
		return 0;
	}
	
	//校验UUID是否支持
	public static boolean isUUIDSupport(UUID uuid){
		String name = mBtSmartUUIDMap.get(uuid);
		if(name != null){
			return true;
		}
		
		return false;
	}
	
	//取服务对应的配置
	public static BtSmartDeviceServerItem getBtServerItem(UUID suuid){
		return mBtSmartMap.get(suuid);
	}
	
	//通过uuid取属性名
	public static String getUUIDName(UUID uuid){
		String tmp = mBtSmartUUIDMap.get(uuid);
		if(tmp == null){
			tmp = "Unknown";
		}
		
		return tmp;
	}
	
	//判断uuid是否是服务的uuid
	public static boolean isServerUUID(UUID uuid){
		BtSmartDeviceServerItem tmp = mBtSmartMap.get(uuid);
		if(tmp == null){
			return false;
		}
		
		return true;
	}
	
	//从xml加载配置参数
	private static boolean FromXMLLoadBtProfile(Context context, String confname) throws IOException
    {
    	//读取配置文件
    	InputStream inStream = context.getAssets().open(confname);
    	
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try 
        {
        	DocumentBuilder builder = factory.newDocumentBuilder();
            Document dom = builder.parse(inStream);
            Element root = (Element) dom.getDocumentElement();
            
            //查找所有Param节点
            NodeList items = root.getElementsByTagName("btProfile");
            for (int i = 0; i < items.getLength(); i++){
            	String server = null;
            	String uuid = null;
            	String des = null;
            	String charpath = null;

                //得到第一个Param节点
                Element personNode = (Element) items.item(i);

                //获取Param节点下的所有子节点(标签之间的空白节点和key/value/type元素)
                NodeList childsNodes = ((Node) personNode).getChildNodes();
                for (int j = 0; j < childsNodes.getLength(); j++) {
                	Node node = (Node) childsNodes.item(j); //判断是否为元素类型
                    if(node.getNodeType() == Node.ELEMENT_NODE){   
                    	Element childNode = (Element) node;
                    	//判断是否name元素
	                    if ("server".equals(childNode.getNodeName())) {
	                    	//获取name元素下Text节点,然后从Text节点获取数据
	                    	server = ((Node) childNode).getFirstChild().getNodeValue();
	                    } 
	                    else if ("uuid".equals(childNode.getNodeName())) {
	                    	uuid = ((Node) childNode).getFirstChild().getNodeValue();
	                    }
	                    else if ("des".equals(childNode.getNodeName())) {
	                    	des = ((Node) childNode).getFirstChild().getNodeValue();
	                    }
	                    else if ("btcharacteristic_path".equals(childNode.getNodeName())) {
	                    	charpath = ((Node) childNode).getFirstChild().getNodeValue();
	                    }
                    }
                 }
                
            	if(server != null && uuid != null && charpath != null){
                   	//创建对象
            		BtSmartDeviceServerItem tmp = new BtSmartDeviceServerItem(server, uuid, des, false);
                	tmp.m_CharacteristicList= FromXmlLoadCharacteristic(context, charpath, tmp); 
                	mBtSmartMap.put(tmp.m_ServerUUID, tmp);
                	if(des != null){
                		server += "_" + des; 
                	}
                	mBtSmartUUIDMap.put(tmp.m_ServerUUID, server);
                	break;
            	}
              }
        } 
        catch (Exception e) 
        {
        	Log.i(TAG, e.getMessage());
        	inStream.close();
        	return false;
        }
        inStream.close();
        
        return true;
    }	
	
	private static ArrayList<BtCharacteristic> FromXmlLoadCharacteristic(Context context, String confname, BtSmartDeviceServerItem tmp) throws IOException{
		ArrayList<BtCharacteristic> paramlist = new ArrayList<BtCharacteristic>();
		
    	//读取配置文件
    	InputStream inStream = context.getAssets().open(confname);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try 
        {
        	DocumentBuilder builder = factory.newDocumentBuilder();
            Document dom = builder.parse(inStream);
            Element root = (Element) dom.getDocumentElement();
            
            //查找所有Param节点
            NodeList items = root.getElementsByTagName("characteristic");
            for (int i = 0; i < items.getLength(); i++) 
            {
                //得到第一个Param节点
                Element personNode = (Element) items.item(i);

            	String name = null;
            	String uuid = null;
            	String des = "";
            	
                //获取Param节点下的所有子节点(标签之间的空白节点和key/value/type元素)
                NodeList childsNodes = ((Node) personNode).getChildNodes();
                for (int j = 0; j < childsNodes.getLength(); j++) 
                {
                	Node node = (Node) childsNodes.item(j); //判断是否为元素类型
                    if(node.getNodeType() == Node.ELEMENT_NODE)
                    {   
                    	Element childNode = (Element) node;
 	
                    	//判断是否name元素
	                    if ("name".equals(childNode.getNodeName())) {
	                    	name = ((Node) childNode).getFirstChild().getNodeValue();
	                    }
	                    else if ("uuid".equals(childNode.getNodeName())) {
	                    	uuid = ((Node) childNode).getFirstChild().getNodeValue();
	                    }
	                    else if ("des".equals(childNode.getNodeName())) 
	                    {
	                    	des = ((Node) childNode).getFirstChild().getNodeValue();
	                    } 
                    }
                 }
                
                //添加数据
              	if(name != null && uuid != null){
            		
              		BtCharacteristic tmpchar = tmp.new BtCharacteristic(name, uuid, des);
              		
                	//获取uuid
                	paramlist.add(tmpchar);  
                	if(des != null){
                		name += "_" + des; 
                	}
                	mBtSmartUUIDMap.put(UUID.fromString(uuid), name);
            	}
                
              }
        } 
        catch (Exception e) 
        {
        	Log.i(TAG, e.getMessage());
        }

        inStream.close();
        return paramlist;
	}
	
	//
	@SuppressWarnings("unused")
	private static Editor getEditor(Context context) 
	{
		SharedPreferences preferences = getSharedPreferences(context);
		return preferences.edit();
	}
    
	//
	private static SharedPreferences getSharedPreferences(Context context) 
	{
		SharedPreferences preferences = context.getSharedPreferences(
										BT_SMART_KEY, 
				                        Context.MODE_PRIVATE);
		return preferences;
	}
	
}
