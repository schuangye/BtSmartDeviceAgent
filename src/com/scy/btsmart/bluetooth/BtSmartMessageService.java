package com.scy.btsmart.bluetooth;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;

import android.annotation.TargetApi;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.ParcelUuid;
import android.util.Log;

import com.scy.btsmart.bluetooth.BtSmartRequest.RequestType;
import com.scy.btsmart.common.BtSmartCommon;
import com.scy.btsmart.common.BtSmartErrorCode;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BtSmartMessageService extends Service implements Runnable{

	private static final String TAG = "BtSmartService";

	private final IBinder mBinder = new LocalBinder();
	
	//响应通知通知队列
	private CharacteristicHandlersContainer mNotificationHandlers = new CharacteristicHandlersContainer();
	
	//消息队列
	private Queue<BtSmartRequest> mRequestQueue = new LinkedList<BtSmartRequest>();
	
	//本地蓝牙设备
	private BluetoothAdapter mBtAdapter = null;
	
	private BluetoothGatt mGattClient = null;	
	private DeviceConnectHandler  mDeviceConHandler = new DeviceConnectHandler(this);
	
	//默认处理handler
	BtSmartDefaultDoHandler mDefaultHandler = new BtSmartDefaultDoHandler(this);
	
	//默认通知处理函数
	BtSmartDefaultNotifyHandler mDefaultNotifyHandler = new BtSmartDefaultNotifyHandler(this);
	
	private boolean  mConnectStatus = false;
	
	private static Object mLock = new Object();
	private BtSmartRequest mCurrentRequest = null;
	
	//线程是否退出
	private boolean  mIsExit = true;

	//启动线程
	Thread mThread = new Thread(this, "BtSmartMessageService");
	
	public class LocalBinder extends Binder {
		public BtSmartMessageService getService() {
			// Return this instance of BtSmartService so clients can call public
			// methods.
			return BtSmartMessageService.this;
		}
	}
	
	@Override
	public void onCreate() {
		//获取蓝牙设备
		if (mBtAdapter == null) {
			mBtAdapter = ((BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
			if(mBtAdapter == null){
				return;
			}
		}
		
		//启动线程
		mThread.start();
	}
	
	@SuppressWarnings("deprecation")
	@Override 
    public void onStart(Intent intent, int startId) { 
            super.onStart(intent, startId); 
    }
    
	@SuppressWarnings("deprecation")
	@Override
	public void onDestroy() 
	{
		//关闭连接
		disConnect();
		
		//退出线程
		mIsExit = false;
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			Log.i(TAG, "Thread exit fail." + e.getMessage()); 
		}
		
		mThread.stop();
		
		super.onDestroy();
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return mBinder;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(mIsExit){
			
			long now = System.currentTimeMillis();
			
			synchronized (mLock){
				if(mCurrentRequest == null ||(!mCurrentRequest.m_IsSynRequest && mCurrentRequest.m_IsFinish) || 
					(mCurrentRequest.m_TimeOut > (now- mCurrentRequest.m_StartTime))){
					
					//处理下一条消息
					processNextRequest();
				}
			}
			
			//等待
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Log.i(TAG, "Thread exit fail." + e.getMessage()); 
			}
		}
	};
    /**
     * 同步设置通知
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param serviceUuid
     *         服务id
     * @param characteristicUuid
     *         属性id
     *  @param timeout
     *         超时时间
     * @return
     */
	public int synSetCharacteristicNotify(int requestId, BluetoothDevice device, UUID serviceUuid, UUID characteristicUuid,
			                              Handler handler, int timeout){
		BtSmartRequest req = new BtSmartRequest(RequestType.CHARACTERISTIC_NOTIFY, requestId, device, serviceUuid, 
				                                characteristicUuid, timeout);
		
		int ret = BtSmartErrorCode.BT_SMART_SUCCEED;
		
		//执行同步命令
		if(!synProcessRequest(req)){
			ret = BtSmartErrorCode.ENABLE_NOTIFY_FAIL;
		}else{
			synchronized (mLock){
				ret = mCurrentRequest.m_ErrCode;
				mCurrentRequest = null;
			}
		}
		
		//添加通知handler
		if(ret == BtSmartErrorCode.BT_SMART_SUCCEED && handler != null){
			synchronized(mNotificationHandlers){
				mNotificationHandlers.addHandler(serviceUuid, characteristicUuid, handler);
			}
		}
		
		return ret;
	}
    /**
     * 异步设置通知
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param serviceUuid
     *         服务id
     * @param characteristicUuid
     *         属性id
     * @param handler
     *         回调通知
     *  @param timeout
     *         超时时间
     * @return
     */
	public void asynSetCharacteristicNotify(int requestId, BluetoothDevice device, UUID serviceUuid, 
			                                UUID characteristicUuid, Handler handler, int timeout) {
		
		boolean isdo = false;
		
		//检查是否空闲
		synchronized (mLock){
			if (mCurrentRequest == null) {
				mCurrentRequest = new BtSmartRequest(RequestType.CHARACTERISTIC_NOTIFY, requestId, device, serviceUuid, 
						                             characteristicUuid, handler, timeout);	
				isdo = true;
			}
		}
		
		if(isdo){
			if(!doNotifyRequest()){
				Log.i(TAG, "doNotifyRequest fail."); 
			}else{
				//添加通知handler
				if(handler != null){
					synchronized(mNotificationHandlers){
						mNotificationHandlers.addHandler(serviceUuid, characteristicUuid, handler);
					}
				}
			}
		}else{
			//添加到消息队列
			synchronized(mRequestQueue){
				mRequestQueue.add(new BtSmartRequest(RequestType.CHARACTERISTIC_NOTIFY, requestId, device, serviceUuid,
	                              characteristicUuid, handler, timeout));
			}
			
			//添加通知handler
			if(handler != null){
				synchronized(mNotificationHandlers){
					mNotificationHandlers.addHandler(serviceUuid, characteristicUuid, handler);
				}
			}
			
		}
	}
    /**
     * 取设备信号强度
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     *  @param timeout
     *         超时时间
     * @return
     */
	public int synGetRemoteRssi(int requestId, BluetoothDevice device, int timeout) {
		BtSmartRequest req = new BtSmartRequest(RequestType.READ_RSSI, requestId, device, timeout);
		
		int rssi = 0;
		
		//执行同步命令
		if(synProcessRequest(req)){
			synchronized (mLock){
				if(mCurrentRequest.m_ResultValue != null){
					rssi = mCurrentRequest.m_Rssi;
				}
				mCurrentRequest = null;
			}
		}
		
		return rssi;
	}
    /**
     * 同步取属性值
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param serviceUuid
     *         服务id
     * @param characteristicUuid
     *         属性id
     *  @param timeout
     *         超时时间
     * @return
     */
	public byte[] synGetCharacteristic(int requestId, BluetoothDevice device, UUID service, UUID characteristic,
			                           int timeout) {
		BtSmartRequest req = new BtSmartRequest(RequestType.READ_CHARACTERISTIC, requestId, device, service, characteristic, timeout);
		
		byte  []tmpresult = null;
		
		//执行同步命令
		if(synProcessRequest(req)){
			synchronized (mLock){
				if(mCurrentRequest.m_ResultValue != null){
					tmpresult = new byte[mCurrentRequest.m_ResultValue.length];
					System.arraycopy(tmpresult, 0, mCurrentRequest.m_ResultValue, 0, mCurrentRequest.m_ResultValue.length);
				}
				mCurrentRequest = null;
			}	
		}

		return tmpresult;
	}
    /**
     * 异步设置通知
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param serviceUuid
     *         服务id
     * @param characteristicUuid
     *         属性id
     * @param handler
     *         回调通知
     *  @param timeout
     *         超时时间
     * @return 
     */
	public void asynGetCharacteristic(int requestId, BluetoothDevice device, UUID service, 
			                               UUID characteristic, Handler handler, int timeout) {
		
		boolean isdo = false;
		
		//检查是否空闲
		synchronized (mLock){
			if (mCurrentRequest == null) {
				mCurrentRequest = new BtSmartRequest(RequestType.READ_CHARACTERISTIC, requestId, device, service, 
                                                     characteristic, handler, timeout);
				isdo = true;
			}
		}
		
		if(isdo){
			if(!doCharacteristicRead()){
				Log.i(TAG, "asynGetCharacteristic fail."); 
			}
		}else{
				//添加到消息队列
				synchronized(mRequestQueue){
					mRequestQueue.add(new BtSmartRequest(RequestType.READ_CHARACTERISTIC, requestId, device, service,
							                             characteristic, handler, timeout));
				}
		}
	}
    /**
     * 同步设置属性值
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param serviceUuid
     *         服务id
     * @param characteristicUuid
     *         属性id
     * @param  value
     *         设置的值
     *  @param timeout
     *         超时时间
     * @return
     */
	public int synSetCharacteristic(int requestId, BluetoothDevice device, UUID service, UUID characteristic,
			                        byte []value, int timeout) {
		if(value == null){
			return -1;
		}
		
		BtSmartRequest req = new BtSmartRequest(RequestType.WRITE_CHARACTERISTIC, requestId, device, service, 
                                                characteristic, value, timeout);

		int ret = BtSmartErrorCode.BT_SMART_SUCCEED;
		
		//执行同步命令
		if(!synProcessRequest(req)){
			ret = BtSmartErrorCode.CHARACTERISTIC_WRITE_FAIL;
		}else{
			synchronized (mLock){
				ret = mCurrentRequest.m_ErrCode;
				mCurrentRequest = null;
			}
		}
		
		return ret;
	}
    /**
     * 异步设置属性值
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param serviceUuid
     *         服务id
     * @param characteristicUuid
     *         属性id
     * @param handler
     *         回调通知    
     * @param  value
     *         设置的值
     *  @param timeout
     *         超时时间
     * @return
     */
	public void asynSetCharacteristic(int requestId, BluetoothDevice device, UUID service, UUID characteristic, 
			                          Handler handler, byte []value, int timeout) {
		if(value == null){
			return;
		}
		
		boolean isdo = false;
		
		//检查是否空闲
		synchronized (mLock){
			if (mCurrentRequest == null) {
				mCurrentRequest = new BtSmartRequest(RequestType.WRITE_CHARACTERISTIC, requestId, device, service, 
                                                     characteristic, handler, value, timeout);
				isdo = true;
			}
		}
		
		if(isdo){
			if(!doCharacteristicWrite()){
				Log.i(TAG, "asynSetCharacteristic fail."); 
			}
		}else{
			//添加到消息队列
			synchronized(mRequestQueue){
				mRequestQueue.add(new BtSmartRequest(RequestType.WRITE_CHARACTERISTIC, requestId, device, service,
						                             characteristic, handler, value, timeout));
			}
		}
	}
    /**
     * 同步取描述值
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param service
     *         服务id
     * @param characteristic
     *         属性id
     * @param descriptor
     *         描述id
     *  @param timeout
     *         超时时间
     * @return 
     */
	public byte[] synGetDescriptor(int requestId, BluetoothDevice device, UUID service, UUID characteristic, UUID descriptor,
			                       int timeout) {
		BtSmartRequest req = new BtSmartRequest(RequestType.READ_DESCRIPTOR, requestId, device, service, characteristic, 
                                                timeout);
		
		byte  []tmpresult = null;
		
		//执行同步命令
		if(synProcessRequest(req)){
			synchronized (mLock){
				if(mCurrentRequest.m_ResultValue != null){
					tmpresult = new byte[mCurrentRequest.m_ResultValue.length];
					System.arraycopy(tmpresult, 0, mCurrentRequest.m_ResultValue, 0, mCurrentRequest.m_ResultValue.length);
				}
				mCurrentRequest = null;
			}		
		}
		
		return tmpresult;
	}
    /**
     * 同步取描述值
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param service
     *         服务id
     * @param characteristic
     *         属性id
     * @param descriptor
     *         描述id
     * @param handler
     *         回调通知   
     *  @param timeout
     *         超时时间
     * @return 
     */
	public void asynGetDescriptor(int requestId, BluetoothDevice device, UUID service, UUID characteristic, UUID descriptor,
			                      Handler handler, int timeout) {
		boolean isdo = false;
		
		//检查是否空闲
		synchronized (mLock){
			if (mCurrentRequest == null) {
				mCurrentRequest = new BtSmartRequest(RequestType.READ_DESCRIPTOR, requestId, device, service, 
                                                     characteristic, handler, timeout);
				isdo = true;
			}
		}
		
		if(isdo){
			if(!doDescriptorRead()){
				Log.i(TAG, "asynGetDescriptor fail."); 
			}
		}else{
			//添加到消息队列
			synchronized(mRequestQueue){
				mRequestQueue.add(new BtSmartRequest(RequestType.READ_DESCRIPTOR, requestId, device, service,
						                             characteristic, handler, timeout));
			}
		}
	}
    /**
     * 同步设置属性值
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param serviceUuid
     *         服务id
     * @param characteristicUuid
     *         属性id 
     * @param descriptor
     *         描述id 
     * @param  value
     *         设置的值
     *  @param timeout
     *         超时时间
     * @return
     */
	public int synSetDescriptor(int requestId, BluetoothDevice device, UUID service, UUID characteristic, UUID descriptor,
			                    byte []value, int timeout) {
		if(value == null){
			return -1;
		}
		
		BtSmartRequest req = new BtSmartRequest(RequestType.WRITE_CHARACTERISTIC, requestId, device, service, 
                                                characteristic, value, timeout);

		int ret = BtSmartErrorCode.BT_SMART_SUCCEED;
		
		//执行同步命令
		if(!synProcessRequest(req)){
			ret = BtSmartErrorCode.DESCRIPTOR_WRITE_FAIL;
		}else{
			synchronized (mLock){
				ret = mCurrentRequest.m_ErrCode;
				mCurrentRequest = null;
			}
		}
		
		return ret;
	}
    /**
     * 异步设置描述值
     * @param requestId
     *         请求id
     * @param device
     *         远程设备
     * @param serviceUuid
     *         服务id
     * @param characteristicUuid
     *         属性id
     * @param descriptor
     *         描述id
     * @param handler
     *         回调通知    
     * @param  value
     *         设置的值
     *  @param timeout
     *         超时时间
     * @return
     */
	public void asynSetDescriptor(int requestId, BluetoothDevice device, UUID service, UUID characteristic, UUID descriptor,
			                      Handler handler, byte []value, int timeout) {
		if(value == null){
			return;
		}
		
		boolean isdo = false;
		
		//检查是否空闲
		synchronized (mLock){
			if (mCurrentRequest == null) {
				mCurrentRequest = new BtSmartRequest(RequestType.WRITE_DESCRIPTOR, requestId, device, service, 
                                                     characteristic, handler, value, timeout);
				isdo = true;
			}
		}
		
		if(isdo){
			if(!doDescriptorWrite()){
				Log.i(TAG, "asynSetDescriptor fail."); 
			}
		}else{
			//添加到消息队列
			synchronized(mRequestQueue){
				mRequestQueue.add(new BtSmartRequest(RequestType.WRITE_DESCRIPTOR, requestId, device, service,
						                             characteristic, handler, value, timeout));
			}
		}		
	}
	/**
	 * 设置连接状态
	 * 
	 * @param status
	 *            设置连接状态
	 */
	private void SetConnectStatus(boolean status){
		mConnectStatus = status;
	}
	/**
	 * Helper function to send a message to a handler with no parameters.
	 * 
	 * @param h
	 *            The Handler to send the message to.
	 * @param msgId
	 *            The message identifier to send. Use one of the defined constants.
	 */
	private void sendMessage(Handler h, int msgId) {
		if (h != null) {
			Message.obtain(h, msgId).sendToTarget();
		}
	}
	/**
	 * Helper function to send a message to a handler with no parameters except the request ID.
	 * 
	 * @param h
	 *            The Handler to send the message to.
	 * @param msgId
	 *            The message identifier to send. Use one of the defined constants.
	 * @param requestId
	 *            The request ID provided by the client of this Service.
	 */
	private void sendMessage(int msgId, int errcode, String errdes) {

		if (mCurrentRequest.m_NotifyHandler != null) {
			Bundle messageBundle = new Bundle();
			Message msg = Message.obtain(mCurrentRequest.m_NotifyHandler, msgId);
			messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, mCurrentRequest.m_SmartDevice.getName());
			messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME,mCurrentRequest.m_SmartDevice.getAddress());
			messageBundle.putInt(BtSmartCommon.EXTRA_REQUEST_ID, mCurrentRequest.m_RequestId);
			messageBundle.putParcelable(BtSmartCommon.EXTRA_SERVICE_UUID, new ParcelUuid(mCurrentRequest.m_ServiceUuid));
			messageBundle.putParcelable(BtSmartCommon.EXTRA_CHARACTERISTIC_UUID, new ParcelUuid(mCurrentRequest.m_CharacteristicUuid));
			messageBundle.putInt(BtSmartCommon.EXTRA_RESULT, errcode);
			if (errdes != null) {
				messageBundle.putString(BtSmartCommon.EXTRA_ErrDES, errdes);
			}
			msg.setData(messageBundle);
			msg.sendToTarget();
		}
	}
	/**
	 * Connect to a remote Bluetooth Smart device. The deviceHandler will receive MESSAGE_CONNECTED on
     * connection success.
	 * 
	 * @param device
	 *            The BluetothDevice to connect to.
	* @param timeout
	 *            connect time out unit Seconds.
	 * @return Boolean success value.
	 */
	private boolean connectAsClient(BluetoothDevice device, int timeout) {
		//断开连接
		disConnect();
		
		//连接设备
		mGattClient = device.connectGatt(this, false, mGattCallbacks);
		
		int count = timeout*1000/500;
		while(count > 0){
			if(mConnectStatus){
				return true;
			}
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				Log.i(TAG, "wait connect fail. " + e.getMessage()); 
			}
		}
		
		return false;
	}
	
	/**
	 * Disconnect from the currently connected Bluetooth Smart device.
	 */
	private void disConnect() {
		if (mGattClient != null) {
			mGattClient.disconnect();
			mGattClient.close();
			mConnectStatus = false;
		}		
	}
	/**
	 * This is where most of the interesting stuff happens in response to changes in BLE state for a client.
	 */
	private BluetoothGattCallback mGattCallbacks = new BluetoothGattCallback() {

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
			if (newState == BluetoothProfile.STATE_CONNECTED && mGattClient != null) {
				
				//返回设备连接状态
				mGattClient.discoverServices();
				sendMessage(mDeviceConHandler, BtSmartCommon.MESSAGE_CONNECTED);
			}else{
				sendMessage(mDeviceConHandler, BtSmartCommon.MESSAGE_DISCONNECTED);
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				// 返回设备支持的服务列表
				sendMessage(mDeviceConHandler, BtSmartCommon.MESSAGE_SERVICE_DISCOVER);
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
			//取通知handler
			Handler notificationHandler = mNotificationHandlers.getHandler(characteristic.getService().getUuid(),characteristic.getUuid());
			if(notificationHandler == null){
				notificationHandler = mDefaultNotifyHandler;
			}
			
			if (notificationHandler != null) {
				Bundle messageBundle = new Bundle();
				Message msg = Message.obtain(notificationHandler, BtSmartCommon.MESSAGE_CHARACTERISTIC_CHANGE);
				messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, gatt.getDevice().getName());
				messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, gatt.getDevice().getAddress());
				messageBundle.putParcelable(BtSmartCommon.EXTRA_SERVICE_UUID, new ParcelUuid(characteristic.getService().getUuid()));
				messageBundle.putParcelable(BtSmartCommon.EXTRA_CHARACTERISTIC_UUID, new ParcelUuid(characteristic.getUuid()));
				messageBundle.putByteArray(BtSmartCommon.EXTRA_VALUE, characteristic.getValue());
				msg.setData(messageBundle);
				msg.sendToTarget();
			}
		}
		
		@Override
		public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status){
			synchronized(mLock){
				if(mCurrentRequest == null){
					return;
				}
				
				if(!checkDeviceSame(mCurrentRequest.m_SmartDevice, gatt.getDevice())){
					return;
				}
				
				if (mCurrentRequest.m_Type == RequestType.READ_RSSI) {
					if(!mCurrentRequest.m_IsSynRequest){
						//mCurrentRequest.m_Rssi = rssi;
					}else{
						mCurrentRequest.m_Rssi = rssi;
						mCurrentRequest.m_ErrCode = status;
					}
					
					//设置消息处理完成
					mCurrentRequest.m_IsFinish = true;
				}else{
					Log.i(TAG, "onDescriptorRead:type error. mCurrentRequest.m_Type:" + mCurrentRequest.m_Type); 
				}
			}
			
			if(status != BluetoothGatt.GATT_SUCCESS){
				Log.i(TAG, "onReadRemoteRssi fail. status:" + status); 
			}
		}
		/**
		 * After calling registerForNotification this callback should trigger, and then we can perform the actual
		 * enable. It could also be called when a descriptor was requested directly, so that case is handled too.
		 */
		@Override
		public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			
			synchronized(mLock){
				if(mCurrentRequest == null){
					return;
				}
				
				if(!checkDeviceSame(mCurrentRequest.m_SmartDevice, gatt.getDevice())){
					return;
				}
				
				if (mCurrentRequest.m_Type == RequestType.READ_DESCRIPTOR) {
					if(!mCurrentRequest.m_IsSynRequest){
						asynDescriptorReadRsp(gatt, descriptor, status);
					}else{
						mCurrentRequest.m_ErrCode = status;
					}
					
					//设置消息处理完成
					mCurrentRequest.m_IsFinish = true;
				}else{
					Log.i(TAG, "onDescriptorRead:type error. mCurrentRequest.m_Type:" + mCurrentRequest.m_Type); 
				}
			}
			
			if(status != BluetoothGatt.GATT_SUCCESS){
				Log.i(TAG, "onDescriptorWrite fail. status:" + status); 
			}
		}

		/**
		 * After writing the CCC for a notification this callback should trigger. It could also be called when a
		 * descriptor write was requested directly, so that case is handled too.
		 */
		@Override
		public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			
			synchronized(mLock){
				if(mCurrentRequest == null){
					return;
				}
				
				//校验设备
				if(!checkDeviceSame(mCurrentRequest.m_SmartDevice, gatt.getDevice())){
					return;
				}
				
				if (mCurrentRequest.m_Type == RequestType.WRITE_DESCRIPTOR) {
					if(!mCurrentRequest.m_IsSynRequest){
						sendMessage(BtSmartCommon.MESSAGE_REQUEST_FINISH, status, null);
					}else{
						mCurrentRequest.m_ErrCode = status;
					}
					
					//设置消息处理完成
					mCurrentRequest.m_IsFinish = true;
				}else{
					Log.i(TAG, "onDescriptorRead:type error. mCurrentRequest.m_Type:" + mCurrentRequest.m_Type); 
				}
			}
			
			if(status != BluetoothGatt.GATT_SUCCESS){
				Log.i(TAG, "onDescriptorWrite fail. status:" + status); 
			}
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {		
			synchronized(mLock){
				if(mCurrentRequest == null){
					return;
				}
				
				if(!checkDeviceSame(mCurrentRequest.m_SmartDevice, gatt.getDevice())){
					return;
				}
				
				if (mCurrentRequest.m_Type == RequestType.READ_CHARACTERISTIC) {
					if(!mCurrentRequest.m_IsSynRequest){
						asynCharacteristicReadRsp(gatt, characteristic, status);
					}else{
						mCurrentRequest.m_ErrCode = status;
					}
					
					//设置消息处理完成
					mCurrentRequest.m_IsFinish = true;
				}else{
					Log.i(TAG, "onDescriptorRead:type error. mCurrentRequest.m_Type:" + mCurrentRequest.m_Type); 
				}
			}
			
			if(status != BluetoothGatt.GATT_SUCCESS){
				Log.i(TAG, "onDescriptorWrite fail. status:" + status); 
			}
		}
		
		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
			
			synchronized(mLock){
				if(mCurrentRequest == null){
					return;
				}
				
				if(!checkDeviceSame(mCurrentRequest.m_SmartDevice, gatt.getDevice())){
					return;
				}
				
				if (mCurrentRequest.m_Type == RequestType.WRITE_CHARACTERISTIC) {
					if(!mCurrentRequest.m_IsSynRequest){
						asynCharacteristicWriteRsp(gatt, characteristic, status);
					}else{
						mCurrentRequest.m_ErrCode = status;
					}
					
					//设置消息处理完成
					mCurrentRequest.m_IsFinish = true;
				}else{
					Log.i(TAG, "onDescriptorRead:type error. mCurrentRequest.m_Type:" + mCurrentRequest.m_Type); 
				}
			}	
			
			if(status != BluetoothGatt.GATT_SUCCESS){
				Log.i(TAG, "onDescriptorWrite fail. status:" + status); 
			}
		}
	};
	
	//异步DescriptorRead处理
	private void asynDescriptorReadRsp(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status){
		BluetoothGattCharacteristic characteristic = descriptor.getCharacteristic();
		if (status != BluetoothGatt.GATT_SUCCESS){
			sendMessage(BtSmartCommon.MESSAGE_REQUEST_FAILED, status, null);
			Log.i(TAG, "asynDescriptorReadRsp fail. status:" + status); 
			return;
		}
		
		Bundle messageBundle = new Bundle();
		Message msg = Message.obtain(mCurrentRequest.m_NotifyHandler, BtSmartCommon.MESSAGE_DESCRIPTOR_VALUE);
		messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, gatt.getDevice().getName());
		messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, gatt.getDevice().getAddress());
		messageBundle.putInt(BtSmartCommon.EXTRA_REQUEST_ID, mCurrentRequest.m_RequestId);
		messageBundle.putParcelable(BtSmartCommon.EXTRA_SERVICE_UUID, new ParcelUuid(characteristic.getService().getUuid()));
        messageBundle.putParcelable(BtSmartCommon.EXTRA_CHARACTERISTIC_UUID, new ParcelUuid(characteristic.getUuid()));
        messageBundle.putParcelable(BtSmartCommon.EXTRA_DESCRIPTOR_UUID, new ParcelUuid(descriptor.getUuid()));
		messageBundle.putByteArray(BtSmartCommon.EXTRA_VALUE, characteristic.getValue());
		msg.setData(messageBundle);
		msg.sendToTarget();
	}
	
	//异步Characteristic处理
	private void asynCharacteristicReadRsp(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
		if (status != BluetoothGatt.GATT_SUCCESS){
			sendMessage(BtSmartCommon.MESSAGE_REQUEST_FAILED, status, null);
			Log.i(TAG, "asynCharacteristicReadRsp fail. status:" + status); 
			return;
		}
		
		Bundle messageBundle = new Bundle();
		Message msg = Message.obtain(mCurrentRequest.m_NotifyHandler, BtSmartCommon.MESSAGE_CHARACTERISTIC_VALUE);
		messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, gatt.getDevice().getName());
		messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, gatt.getDevice().getAddress());
		messageBundle.putInt(BtSmartCommon.EXTRA_REQUEST_ID, mCurrentRequest.m_RequestId);
		messageBundle.putByteArray(BtSmartCommon.EXTRA_VALUE, characteristic.getValue());
		messageBundle.putParcelable(BtSmartCommon.EXTRA_SERVICE_UUID, new ParcelUuid(characteristic.getService().getUuid()));
        messageBundle.putParcelable(BtSmartCommon.EXTRA_CHARACTERISTIC_UUID, new ParcelUuid(characteristic.getUuid()));	                    
		msg.setData(messageBundle);
		msg.sendToTarget();
	}
	//异步Characteristic 写处理
	private void asynCharacteristicWriteRsp(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
		if (status != BluetoothGatt.GATT_SUCCESS){
			sendMessage(BtSmartCommon.MESSAGE_REQUEST_FAILED, status, null);
			Log.i(TAG, "asynCharacteristicReadRsp fail. status:" + status); 
			return;
		}
		
		Bundle messageBundle = new Bundle();
		Message msg = Message.obtain(mCurrentRequest.m_NotifyHandler, BtSmartCommon.MESSAGE_WRITE_COMPLETE);
		messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, gatt.getDevice().getName());
		messageBundle.putString(BtSmartCommon.EXTRA_DEVICE_NAME, gatt.getDevice().getAddress());
		messageBundle.putByteArray(BtSmartCommon.EXTRA_VALUE, characteristic.getValue());
		messageBundle.putParcelable(BtSmartCommon.EXTRA_SERVICE_UUID, new ParcelUuid(characteristic.getService().getUuid()));
        messageBundle.putParcelable(BtSmartCommon.EXTRA_CHARACTERISTIC_UUID, new ParcelUuid(characteristic.getUuid()));	                    
		messageBundle.putInt(BtSmartCommon.EXTRA_REQUEST_ID, mCurrentRequest.m_RequestId);
		msg.setData(messageBundle);
		msg.sendToTarget();
	}
	
	private boolean checkDeviceSame(BluetoothDevice device1, BluetoothDevice device2){
		if(device1.getName().equals(device2.getName()) && 
		  device1.getAddress().equals(device2.getAddress())){
			return true;
		}
		
		return false;
	}
	
	//同步请求处理
	private boolean synProcessRequest(BtSmartRequest req) {
		if(req == null){
			return false;
		}
		
		boolean istimeout = true;
		int count = req.m_TimeOut*1000/100;
		while(count > 0){
			synchronized(mLock){
				if(mCurrentRequest == null){
					mCurrentRequest = req;
					//连接设备
					if(!connectAsClient(mCurrentRequest.m_SmartDevice, BtSmartCommon.CONNECT_TIME_OUT)){
						Log.i(TAG, "Connect device fail. " + mCurrentRequest.m_SmartDevice.getAddress());
						mCurrentRequest = null;
						return false;
					}
					
					istimeout = false;
					break;
				}
			}
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Log.i(TAG, "Thread.sleep(100) fail. " + e.getMessage());
			}
			
			count--;
		}
		
		//执行命令
		if(!istimeout){
			boolean ret = false;
			switch (req.m_Type) {
			case CHARACTERISTIC_NOTIFY:
				ret = doNotifyRequest();
				break;
			case READ_CHARACTERISTIC:
				ret = doCharacteristicRead();
				break;
			case READ_DESCRIPTOR:
				ret = doDescriptorRead();
				break;
			case WRITE_CHARACTERISTIC:
				ret = doCharacteristicWrite();
			default:
				break;
			}
			
			//执行失败返回
			if(!ret){
				return true;
			}
			
			while(count > 0){
				synchronized(mLock){
					if(mCurrentRequest.m_IsFinish){
						return true;
					}
				}
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					Log.i(TAG, "Thread.sleep(100) fail. " + e.getMessage());
				}
				
				count--;			
			}
		}
		
		//超时处理
		synchronized(mLock){
			Log.i(TAG, "synProcessRequest time out. " + mCurrentRequest.m_SmartDevice.getAddress());
			mCurrentRequest = null;
			return false;
		}
	}
	/**
	 * Process the next request in the queue for some BLE action (such as characteristic read). This is required because
	 * the Android 4.3 BLE stack only allows one active request at a time.
	 */
	private void processNextRequest() {
		if (mRequestQueue.isEmpty()) {
			return;
		}
		
		boolean ret = false;
		
		//获取请求
		mCurrentRequest = mRequestQueue.remove();
		
		//连接设备
		if(!connectAsClient(mCurrentRequest.m_SmartDevice, BtSmartCommon.CONNECT_TIME_OUT)){
			Log.i(TAG, "Connect device fail. " + mCurrentRequest.m_SmartDevice.getAddress());
			processNextRequest();
		}

		switch (mCurrentRequest.m_Type) {
		case CHARACTERISTIC_NOTIFY:
			ret = doNotifyRequest();
			break;
		case READ_CHARACTERISTIC:
			ret = doCharacteristicRead();
			break;
		case READ_DESCRIPTOR:
			ret = doDescriptorRead();
			break;
		case WRITE_CHARACTERISTIC:
			ret = doCharacteristicWrite();
		case READ_RSSI:
			ret = doReadRemoteRssi();
		default:
			break;
		}
		
		if(!ret){
			processNextRequest();
		}
	}
	
	//失败处理
	private void SendErrorResult(int errcode, String errdes){
		
		if(!mCurrentRequest.m_IsSynRequest){
			sendMessage(BtSmartCommon.MESSAGE_REQUEST_FAILED, errcode, errdes);
		}else{
			mCurrentRequest.m_ErrCode = errcode;
		}
		
		mCurrentRequest.m_IsFinish = true;
	}
	/**
	 * Perform the notification request now.
	 * 
	 */
	private boolean doNotifyRequest() {
		
		boolean enable = true;
		UUID serviceuuid = null;
		UUID CharacteristicUuid = null;
		
		synchronized(mLock){
    		if(mGattClient == null){
    			mCurrentRequest.m_ErrCode = BtSmartErrorCode.ENABLE_NOTIFY_FAIL;
    			Log.i(TAG, "mGattClient is null");
    			return false;
    		}
    		
			BluetoothGattService serviceObject = mGattClient.getService(mCurrentRequest.m_ServiceUuid);
			if(serviceObject == null){
				SendErrorResult(BtSmartErrorCode.SERVICE_UUID_NOEXIST, null);
				return false;
			}
			
			BluetoothGattCharacteristic Characteristic = serviceObject.getCharacteristic(mCurrentRequest.m_CharacteristicUuid);
			if(Characteristic == null){
				SendErrorResult(BtSmartErrorCode.CHARACTERISTIC_UUID_NOEXIST, null);
				return false;
			}
			
			enable = (mCurrentRequest.m_Value[0] > 0)?true:false;
			//设置通知
			if (!enableNotify(Characteristic, enable)) {
				SendErrorResult(BtSmartErrorCode.ENABLE_NOTIFY_FAIL, null);
				mCurrentRequest.m_IsFinish = true;
				return false;
			}else{
				mCurrentRequest.m_IsFinish = true;
			}
			
			serviceuuid = mCurrentRequest.m_ServiceUuid;
			CharacteristicUuid = mCurrentRequest.m_CharacteristicUuid;
		}
		
		//停止上报,移除事件
		if(!enable){
			synchronized(mNotificationHandlers){
				mNotificationHandlers.removeHandler(serviceuuid, CharacteristicUuid);
			}
		}
		
		return true;
	}
	/**
	 * Perform the characteristic value request now.
	 * 
	 */
	private boolean doReadRemoteRssi(){
		synchronized(mLock){
    		if(mGattClient == null){
    			mCurrentRequest.m_ErrCode = BtSmartErrorCode.READ_RSSI_FAIL;
    			Log.i(TAG, "mGattClient is null");
    			return false;
    		}
    		
			if (!mGattClient.readRemoteRssi()) {
				SendErrorResult(BtSmartErrorCode.READ_RSSI_FAIL, null);
                return false;
            }
		}
		
		return true;
	}
	
	/**
	 * Perform the characteristic value request now.
	 * 
	 */
	private boolean doCharacteristicRead() {
		synchronized(mLock){
    		if(mGattClient == null){
    			mCurrentRequest.m_ErrCode = BtSmartErrorCode.CHARACTERISTIC_READ_FAIL;
    			Log.i(TAG, "mGattClient is null");
    			return false;
    		}
    		
			BluetoothGattService serviceObject = mGattClient.getService(mCurrentRequest.m_ServiceUuid);
			if(serviceObject == null){
				SendErrorResult(BtSmartErrorCode.SERVICE_UUID_NOEXIST, null);
				return false;
			}
			
			BluetoothGattCharacteristic Characteristic = serviceObject.getCharacteristic(mCurrentRequest.m_CharacteristicUuid);
			if(Characteristic == null){
				SendErrorResult(BtSmartErrorCode.CHARACTERISTIC_UUID_NOEXIST, null);
				return false;
			}
			
			if (!mGattClient.readCharacteristic(Characteristic)) {
				SendErrorResult(BtSmartErrorCode.CHARACTERISTIC_READ_FAIL, null);
				return false;
			}
		}
		
		return true;
	}
	/**
	 * 写数据 the charactersitic write now.
	 * 
	 */
    private boolean doCharacteristicWrite() {
    	synchronized(mLock){
    		if(mGattClient == null){
    			mCurrentRequest.m_ErrCode = BtSmartErrorCode.CHARACTERISTIC_WRITE_FAIL;
    			Log.i(TAG, "mGattClient is null");
    			return false;
    		}
    		
    		BluetoothGattService serviceObject = mGattClient.getService(mCurrentRequest.m_ServiceUuid);
			if(serviceObject == null){
				SendErrorResult(BtSmartErrorCode.SERVICE_UUID_NOEXIST, null);
				return false;
			}
			
			BluetoothGattCharacteristic Characteristic = serviceObject.getCharacteristic(mCurrentRequest.m_CharacteristicUuid);
			if(Characteristic == null){
				SendErrorResult(BtSmartErrorCode.CHARACTERISTIC_UUID_NOEXIST, null);
				return false;
			}
			
			Characteristic.setValue(mCurrentRequest.m_Value);
			if (!mGattClient.writeCharacteristic(Characteristic)) {
				SendErrorResult(BtSmartErrorCode.CHARACTERISTIC_WRITE_FAIL, null);
                return false;
            }
    	}
        
        return true;
    }
	/**
	 * Perform the descriptor value request now.
	 * 
	 */
	private boolean doDescriptorRead() {
		synchronized(mLock){
    		if(mGattClient == null){
    			mCurrentRequest.m_ErrCode = BtSmartErrorCode.DESCRIPTOR_READ_FAIL;
    			Log.i(TAG, "mGattClient is null");
    			return false;
    		}
    		
			BluetoothGattService serviceObject = mGattClient.getService(mCurrentRequest.m_ServiceUuid);
			if(serviceObject == null){
				SendErrorResult(BtSmartErrorCode.SERVICE_UUID_NOEXIST, null);
				return false;
			}
			
			BluetoothGattCharacteristic Characteristic = serviceObject.getCharacteristic(mCurrentRequest.m_CharacteristicUuid);
			if(Characteristic == null){
				SendErrorResult(BtSmartErrorCode.CHARACTERISTIC_UUID_NOEXIST, null);
				return false;
			}
			
			BluetoothGattDescriptor descriptorObject = Characteristic.getDescriptor(mCurrentRequest.m_DescriptorUuid);
			if(descriptorObject == null){
				SendErrorResult(BtSmartErrorCode.DESCRIPTOR_UUID_NOEXIST, null);
				return false;
			}
			
			if (!mGattClient.readDescriptor(descriptorObject)) {
				SendErrorResult(BtSmartErrorCode.DESCRIPTOR_READ_FAIL, null);
				return false;
			}
		}
		
		return true;
	}
	/**
	 * Perform the charactersitic write now.
	 * 
	 */
    private boolean doDescriptorWrite() {
    	synchronized(mLock){
    		if(mGattClient == null){
    			mCurrentRequest.m_ErrCode = BtSmartErrorCode.DESCRIPTOR_WRITE_FAIL;
    			Log.i(TAG, "mGattClient is null");
    			return false;
    		}
    		
    		BluetoothGattService serviceObject = mGattClient.getService(mCurrentRequest.m_ServiceUuid);
			if(serviceObject == null){
				SendErrorResult(BtSmartErrorCode.SERVICE_UUID_NOEXIST, null);
				return false;
			}
			
			BluetoothGattCharacteristic Characteristic = serviceObject.getCharacteristic(mCurrentRequest.m_CharacteristicUuid);
			if(Characteristic == null){
				SendErrorResult(BtSmartErrorCode.CHARACTERISTIC_UUID_NOEXIST, null);
				return false;
			}
			
			BluetoothGattDescriptor descriptorObject = Characteristic.getDescriptor(mCurrentRequest.m_DescriptorUuid);
			if(descriptorObject == null){
				SendErrorResult(BtSmartErrorCode.DESCRIPTOR_UUID_NOEXIST, null);
				return false;
			}
			
			descriptorObject.setValue(mCurrentRequest.m_Value);
            if (!mGattClient.writeDescriptor(descriptorObject)) {
            	SendErrorResult(BtSmartErrorCode.DESCRIPTOR_WRITE_FAIL, null);
            	return false;
            }
    	}
    	
        return true;
    }
	/**
	 * Write to the CCC to enable or disable notifications.
	 * 
	 * @param enable
	 *            Boolean indicating whether the notification should be enabled or disabled.
	 * @param characteristic
	 *            The CCC to write to.
	 * @return Boolean result of operation.
	 */
	private boolean enableNotify(BluetoothGattCharacteristic characteristic, boolean enable) {
		if (mGattClient == null) {
			throw new NullPointerException("GATT client not started.");
		}
		
		if (!mGattClient.setCharacteristicNotification(characteristic, enable)) {
			return false;
		}
		
		/*
		BluetoothGattDescriptor clientConfig = characteristic.getDescriptor(BtSmartCommon.CCC);
		if (clientConfig == null) {
			return false;
		}
		
		if (enable){
			clientConfig.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
		} else{
			clientConfig.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
		}

		return mGattClient.writeDescriptor(clientConfig);*/
		return true;
	}
	

    private static class DeviceConnectHandler extends Handler {
        private final WeakReference<BtSmartMessageService> mSmartService;

        public DeviceConnectHandler(BtSmartMessageService service) {
        	mSmartService = new WeakReference<BtSmartMessageService>(service);
        }

        @Override
        public void handleMessage(Message msg) {
        	BtSmartMessageService parentservice = mSmartService.get();
            if (parentservice != null) {
                switch (msg.what) {                
                case BtSmartCommon.MESSAGE_CONNECTED: {
                    // Cancel the connect timer.
                	parentservice.SetConnectStatus(true);
                    }
                    break;
                case BtSmartCommon.MESSAGE_DISCONNECTED: {
                    // End this activity and go back to scan results view.
                	parentservice.SetConnectStatus(false);
                    break;
                }
              }
            }
        }
    }
}
