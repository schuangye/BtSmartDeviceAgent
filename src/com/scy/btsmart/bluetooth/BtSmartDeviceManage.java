/*
 * Copyright: (C)2009 VASDAQ Japan,ltd. All Rights Reserved. 
 * 
 * Author: scy Opensource Software Inc.
 *
 * License: GPL2.0
 *
 */
package com.scy.btsmart.bluetooth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class BtSmartDeviceManage{
	//定义远程蓝牙设备
	private static Map<String, BtSmartDevice> mBtSmartMap = new HashMap<String, BtSmartDevice>();
    /**
     * Add a new device.
     * 
     * @param device 设备对象   
     * @param isremove 是否删除重复
    */
    public static boolean addSmartDevice(BtSmartDevice device, boolean isremove) {
    	
    	String key = device.mDeviceAddress;
    	synchronized(mBtSmartMap){
	    	BtSmartDevice tmpdevice = mBtSmartMap.get(key);
	        if (tmpdevice == null) {
	            mBtSmartMap.put(key, device);
	            return true;
	        }else{
	        	if(isremove == true){
	        		mBtSmartMap.remove(key);
	        		mBtSmartMap.put(key, device);
	        		return true;
	        	}
	        }
    	}
        return false;
    }
    /**
     * remove device.
     * 
     * @param name 设备对象   
     * @param address
    */
    public static void removeSmartDevice(String address) {
    	String key = address;
    	synchronized(mBtSmartMap){
	    	BtSmartDevice tmpdevice = mBtSmartMap.get(key);
	        if (tmpdevice != null) {
	        	mBtSmartMap.remove(key);
	        }
    	}
    }
    /**
     * get device list
     * 
     * 
     *  
    */
    public static List<BtSmartDevice> getSmartDeviceList() {
    	List<BtSmartDevice> devicelist = new ArrayList<BtSmartDevice>();  
    	
        Iterator<BtSmartDevice> it = mBtSmartMap.values().iterator();  
        while (it.hasNext()) { 
        	devicelist.add(it.next());  
        }
        
        if(devicelist.isEmpty()){
        	return null;
        }
        
        return devicelist;
    }
    /**
     * get device.
     * 
     * @param name 设备对象   
     * @param address
    */
    public static BtSmartDevice getSmartDevice(String address) {
    	String key = address;
    	BtSmartDevice tmpdevice = mBtSmartMap.get(key);
        if (tmpdevice != null) {
        	return tmpdevice;
        }
        
        return null;
    }
    /**
     * get device count.
     * 
     * @param 
     * @param 
    */
    public static int getCount() {
        return mBtSmartMap.size();
    }
}
